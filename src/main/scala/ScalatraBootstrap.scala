import com.dexter.imock.controller.{ForgotPassServlet, HomeServlet, LoginServlet, RegistrationServlet}
import javax.servlet.ServletContext
import org.scalatra._

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext): Unit = {
    context.mount(new LoginServlet, "/*")
    context.mount(new RegistrationServlet, "/reg")
    context.mount(new HomeServlet, "/home")
    context.mount(new ForgotPassServlet, "/forgotpass")
  }
}
