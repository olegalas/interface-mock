package com.dexter.imock.controller

import org.scalatra.{BadRequest, ScalatraServlet}
import org.scalatra.forms._
import org.scalatra.i18n.I18nSupport
import org.slf4j.{Logger, LoggerFactory}

class ForgotPassServlet extends ScalatraServlet with FormSupport with I18nSupport {

  val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  val form: MappingValueType[ForgotPass] = mapping(
    "email" -> label("Email", text(required, pattern(".+@.+")))
  )(ForgotPass)

  get("/") {
    LOG.info("FORGOT PASS")
    html.forgotpass()
  }

  post("/") {
    validate(form)(
      errors => {
        LOG.info(s"WAS ERROR. $errors")
        BadRequest(html.forgotpass())
      },
      form   => {
        LOG.info(s"OK : $form")
        html.apply("/", "Password was sent. Please check your email")
      }
    )
  }

}
case class ForgotPass(email: String)
