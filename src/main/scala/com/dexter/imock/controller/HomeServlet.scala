package com.dexter.imock.controller

import com.dexter.imock.model.Scenario
import com.dexter.imock.util.{Login, Logout, SecurityUtil}
import org.json4s.{DefaultFormats, Formats, JValue}
import org.scalatra.ScalatraServlet
import org.scalatra.json._
import org.slf4j.{Logger, LoggerFactory}

class HomeServlet extends ScalatraServlet  with JacksonJsonSupport {

  val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  protected implicit lazy val jsonFormats: Formats = DefaultFormats
  protected override def transformResponseBody(body: JValue): JValue = body.underscoreKeys

  before() {
    contentType = formats("json")
  }

  get("/scenario/:id") {

    SecurityUtil > request match {
      case _@Login =>
        LOG.info(s"Was received scenario id ${params("id")}")
        Scenario("scenarioId", "POST", List("segment1", "segment2"), List("param1" -> "value1", "param2" -> "value2"), List("header1" -> "header2", "header2" -> "header2"), "body", "responseBody")
      case _@Logout =>
        html.login()
    }

  }

  get("/scenario") {

    contentType = formats("json")
    SecurityUtil > request match {
      case _@Login =>
        contentType="text/html"
        html.scenario()
      case _@Logout =>
        html.login()
    }

  }

  put("/scenario") {

    LOG.info("SCENARIO WAS PUT")

    """{"status":"OK"}"""
  }

}
