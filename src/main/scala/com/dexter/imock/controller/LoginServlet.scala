package com.dexter.imock.controller

import com.dexter.imock.model.SpecDTO
import com.dexter.imock.util.{Login, Logout, SecurityUtil}
import com.dexter.imock.util.SecurityUtil.COOKIES_NAME
import org.scalatra._
import org.scalatra.i18n.I18nSupport
import org.scalatra.forms._
import org.slf4j.{Logger, LoggerFactory}

class LoginServlet extends ScalatraServlet with FormSupport with I18nSupport {

  val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  val form: MappingValueType[Credentials] = mapping(
    "login" -> label("Login", text(required, minlength(3))),
    "pass" -> label("Pass", text(required, minlength(3)))
  )(Credentials)

  get("/") {

    SecurityUtil > request match {
      case _@Login =>
        // get spec -> transform to specDTO -> send as parameter to home page
        html.home(SpecDTO("specId", List("scenario 1", "scenario 1", "scenario 1")))
      case _@Logout =>
        html.login()
    }

  }

  post("/") {
    validate(form)(
      errors => {
        LOG.info(s"WAS ERROR. $errors")
        BadRequest(html.login())
      },
      form   => {
        LOG.info(s"OK : $form")
        response.addCookie(new Cookie(COOKIES_NAME, form.pass))

        // get spec -> transform to specDTO -> send as parameter to home page
        html.home(SpecDTO("specId", List("scenario 1", "scenario 1", "scenario 1")))
      }
    )
  }

  delete("/") {
    response.addCookie(new Cookie(COOKIES_NAME, "")(CookieOptions(maxAge = 0)))
  }

}
case class Credentials(login: String, pass: String)