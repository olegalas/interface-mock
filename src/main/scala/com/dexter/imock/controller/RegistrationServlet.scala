package com.dexter.imock.controller

import org.scalatra.{BadRequest, ScalatraServlet}
import org.scalatra.forms._
import org.scalatra.i18n.I18nSupport
import org.slf4j.{Logger, LoggerFactory}

class RegistrationServlet extends ScalatraServlet with FormSupport with I18nSupport {

  val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  val form: ValueType[Registration] = mapping(
    "login" -> label("Login", text(required, minlength(3))),
    "pass" -> label("Pass", text(required, minlength(3))),
    "pass2" -> label("Pass", text(required, minlength(3))),
    "email" -> label("Email", text(required, pattern(".+@.+")))
  )(Registration).verifying { reg =>
    if(reg.pass != reg.pass2) {
      Seq("pass2" -> "Passwords must be the same")
    } else {
      Nil
    }
  }

  get("/") {
    LOG.info("REG")
    html.reg()
  }

  post("/") {
    LOG.info("REG POST")
    validate(form)(
      errors => {
        LOG.info(s"WAS ERROR. $errors")
        BadRequest(html.reg())
      },
      form   => {
        LOG.info(s"OK : $form")
        html.apply("/", "You was successfully registered")
      }
    )
  }

}
case class Registration(login: String, pass: String, pass2: String, email: String)