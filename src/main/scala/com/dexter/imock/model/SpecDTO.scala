package com.dexter.imock.model

case class SpecDTO(specId: String, scenario: List[String])