package com.dexter.imock.model

case class Specification(specId:    String, scenario:  List[Scenario])
case class Scenario(
                     scenarioId:    String,
                     method:        String,
                     segments:      List[String],
                     params:        List[(String, String)],
                     headers:       List[(String, String)],
                     body:          String,
                     responseBody:  String
                   )