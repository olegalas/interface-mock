package com.dexter.imock.util

import org.scalatra.servlet.RichRequest

object SecurityUtil {
  final val COOKIES_NAME = "auth"
  def > (implicit req: RichRequest): Logged = {
    if(req.cookies.exists(t => t._1 == COOKIES_NAME)) Login
    else Logout
  }
}
sealed trait Logged
object Login extends Logged
object Logout extends Logged

