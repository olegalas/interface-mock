$(window).on("load", () => {
    $(".nav-stacked li.scenario-li a").on("click", () => {

        const tableBody = $("#scenario");
        tableBody.empty();


        $.get("/home/scenario/2", (data) => {

            // Creating method
            tableBody.append("<tr><th>Method</th><th>" + data.method + "</th></tr>");

            // Creating segments
            const segments = data.segments.map(segment => "/" + segment + "<br>").reduce((s1, s2) => s1 + s2);
            tableBody.append("<tr><th>Segments</th><th>" + segments + "</th></tr>");

            // Creating params
            const params = data.params.map(param => {
                const paramName = Object.keys(param)[0];
                return paramName + " -> " + param[paramName] + "<br>";
            }).reduce((s1, s2) => s1 + s2);
            tableBody.append("<tr><th>URI params</th><th>" + params + "</th></tr>");

            // Creating headers
            const headers = data.headers.map(param => {
                const paramName = Object.keys(param)[0];
                return paramName + " -> " + param[paramName] + "<br>";
            }).reduce((s1, s2) => s1 + s2);
            tableBody.append("<tr><th>Headers</th><th>" + headers + "</th></tr>");

            // Creating body
            tableBody.append("<tr><th>Request body</th><th>" + data.body + "</th></tr>");

            // Creating response body
            tableBody.append("<tr><th>Response body</th><th>" + data.response_body + "</th></tr>");
        });

    });

    $("#logout").on("click", () => {
        $.ajax({
            url: '/',
            type: 'DELETE',
            success: (result) => {
                window.location = "/"
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.error("Error during logout", textStatus);
                $('#logout-modal').modal('toggle');
            }
        });
    });
});