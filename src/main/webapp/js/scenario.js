$(window).on("load", () => {

    const scenarioPutUrl = "scenario";

    let segments = 0;
    let params = 0;
    let headers = 0;
    let isRequestBody = false;
    let isResponseBody = false;

    class Tuple {
        constructor(k, v) {
            this.k = k;
            this.v = v;
        }
    }

    $(".navbar-brand").on("click", () => {
        window.location = "/"
    });

    $("#addPathParam").on("click", () => {

        const segmentId = "segment" + segments;
        const commonId = "commonDiv" + segmentId;
        $("#pathPlaceholder").after(() => {

            return $("<div class=\"form-group\" id=\"" + commonId + "\"></div>")
                .append("<div class=\"col-sm-2\"></div>")
                .append("<label class=\"col-sm-1 control-label\" for=\"" + segmentId +"\">Parameter</label>")
                .append(() => {
                    return $("<div class=\"col-sm-7\"></div>")
                        .append("<input type=\"text\" id=\"" + segmentId + "\" name=\"" + segmentId + "\" class=\"form-control\" autocomplete=\"" + segmentId + "\">")
                        .append("<span class=\"error\"></span>");
                })
                .append(() => {
                    return $("<div class=\"col-sm-2\"></div>")
                        .append(() => {

                            return $("<input type=\"button\" class=\"btn btn-default\" value=\"remove\">")
                                .on("click", () => {
                                    segments--;
                                    $("#" + commonId).remove();
                                });

                        })
                });
        });
        segments++;

    });

    $("#addURLParam").on("click", () => {

        const paramsId = "param" + params;
        const valueId = "value" + params;
        const commonId = "commonDiv" + paramsId;
        $("#urlPlaceholder").after(() => {

            return $("<div class=\"form-group\" id=\"" + commonId + "\"></div>")
                .append("<div class=\"col-sm-2\"></div>")
                .append("<label class=\"col-sm-1 control-label\" for=\"" + paramsId +"\">Param</label>")
                .append(() => {
                    return $("<div class=\"col-sm-3\"></div>")
                        .append("<input type=\"text\" id=\"" + paramsId + "\" name=\"" + paramsId + "\" class=\"form-control\" autocomplete=\"" + paramsId + "\">")
                        .append("<span class=\"error\"></span>");
                })
                .append("<label class=\"col-sm-1 control-label\" for=\"" + valueId +"\">Value</label>")
                .append(() => {
                    return $("<div class=\"col-sm-3\"></div>")
                        .append("<input type=\"text\" id=\"" + valueId + "\" name=\"" + valueId + "\" class=\"form-control\" autocomplete=\"" + valueId + "\">")
                        .append("<span class=\"error\"></span>");
                })
                .append(() => {
                    return $("<div class=\"col-sm-2\"></div>")
                        .append(() => {

                            return $("<input type=\"button\" class=\"btn btn-default\" value=\"remove\">")
                                .on("click", () => {
                                    params--;
                                    $("#" + commonId).remove();
                                });

                        })
                });
        });

        params++;
    });

    $("#addHeader").on("click", () => {

        const paramsId = "key" + headers;
        const valueId = "headerValue" + headers;
        const commonId = "commonDiv" + paramsId;
        $("#headersPlaceholder").after(() => {

            return $("<div class=\"form-group\" id=\"" + commonId + "\"></div>")
                .append("<div class=\"col-sm-2\"></div>")
                .append("<label class=\"col-sm-1 control-label\" for=\"" + paramsId +"\">Key</label>")
                .append(() => {
                    return $("<div class=\"col-sm-3\"></div>")
                        .append("<input type=\"text\" id=\"" + paramsId + "\" name=\"" + paramsId + "\" class=\"form-control\" autocomplete=\"" + paramsId + "\">")
                        .append("<span class=\"error\"></span>");
                })
                .append("<label class=\"col-sm-1 control-label\" for=\"" + valueId +"\">Value</label>")
                .append(() => {
                    return $("<div class=\"col-sm-3\"></div>")
                        .append("<input type=\"text\" id=\"" + valueId + "\" name=\"" + valueId + "\" class=\"form-control\" autocomplete=\"" + valueId + "\">")
                        .append("<span class=\"error\"></span>");
                })
                .append(() => {
                    return $("<div class=\"col-sm-2\"></div>")
                        .append(() => {

                            return $("<input type=\"button\" class=\"btn btn-default\" value=\"remove\">")
                                .on("click", () => {
                                    params--;
                                    $("#" + commonId).remove();
                                });

                        })
                });
        });

        headers++;
    });

    $("#addRequestBody").on("click", () => {
        isRequestBody = true;
        $("#addRequestBody").hide();
        $("#requestBodyLabel").attr("for","requestBody");
        $("#requestBody").show();
        $("#hideRequestBody").removeClass("hide-element");
    });

    $("#hideRequestBody").on("click", () => {
        isRequestBody = false;
        $("#addRequestBody").show();
        $("#requestBodyLabel").attr("for","addRequestBody");
        $("#requestBody").hide();
        $("#hideRequestBody").addClass("hide-element");
    });

    $("#addResponseBody").on("click", () => {
        isResponseBody = true;
        $("#addResponseBody").hide();
        $("#responseBodyLabel").attr("for","responseBody");
        $("#responseBody").show();
        $("#hideResponseBody").removeClass("hide-element");
    });

    $("#hideResponseBody").on("click", () => {
        isResponseBody = false;
        $("#addResponseBody").show();
        $("#responseBodyLabel").attr("for","addResponseBody");
        $("#responseBody").hide();
        $("#hideResponseBody").addClass("hide-element");
    });

    $("#submitScenario").on("click", (event) => {
        event.preventDefault();


        let segmentsParams = [];
        for(let i = 0; i < segments; i++) {
            segmentsParams.push($("#segment" + i).val());
        }

        let urlParams = [];
        for(let i = 0; i < params; i++) {
            urlParams.push(new Tuple($("#param" + i).val(), $("#value" + i).val()));
        }

        let headersParams = [];
        for(let i = 0; i < params; i++) {
            headersParams.push(new Tuple($("#key" + i).val(), $("#headerValue" + i).val()));
        }

        const scenario = {

            scenarioId: $("#scenarioId").val(),
            method: $("input[name=method]:checked").val(),
            segments: segmentsParams,
            params: urlParams,
            headers: headersParams,
            body: isRequestBody ? $("#requestBody").val() : undefined,
            responseBody: isResponseBody ? $("#responseBody").val() : undefined

        };

        console.log("Scenario !!!", scenario);

        $.ajax({
            url: scenarioPutUrl,
            type: 'PUT',
            data: JSON.stringify(scenario),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: (result) => {
                window.location = "/"
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.error("Error during put", textStatus);
            }
        });

    });

});