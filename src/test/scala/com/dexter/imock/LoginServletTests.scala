package com.dexter.imock

import com.dexter.imock.controller.LoginServlet
import org.scalatra.test.scalatest._

class LoginServletTests extends ScalatraFunSuite {

  addServlet(classOf[LoginServlet], "/*")

  test("GET / on IMockServlet should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

}
